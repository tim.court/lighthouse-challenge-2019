const powerOn = () => { ship.powerOn = true }
const countModules = () => availableModules.length
const countEssential = () => availableModules.filter(m => m.essential).length
const loadModule = (index) => { 
  const m = availableModules[index]
  m.enabled = true
  ship.modules.push(m)
}
const findModuleIndex = (name) => availableModules.findIndex(m => m.name === name)
const resetLARRY = () => { for (let i=0; i<10; i+=1) { LARRY.quack() } }
const setMessage = () => { radio.message = JSON.stringify(navigation) }
const activateBeacon = () => { radio.beacon = true }
const setFrequency = () => { radio.frequency = (radio.range.low + radio.range.high) / 2 }
const initialize = () => {
  navigation.x = 0
  navigation.y = 0
  navigation.z = 0
}
const calibrateX = () => {
  for (let i=1; i<=12; i+=1) {
    navigation.x = checkSignal()
    if (navigation.x !== undefined) {
      return
    }
  }
}

loadModule(findModuleIndex('life-support'))
loadModule(findModuleIndex('propulsion'))
loadModule(findModuleIndex('navigation'))
loadModule(findModuleIndex('communication'))
resetLARRY()
setMessage()

