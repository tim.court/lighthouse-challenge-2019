powerOn = () => { ship.powerOn = true }
countModules = () => availableModules.length
countEssential = () => availableModules.filter(m => m.essential).length
loadModule = (index) => { 
  const m = availableModules[index]
  m.enabled = true
  ship.modules.push(m)
}

loadModule(availableModules.findIndex(m => m.name === 'life-support'))
