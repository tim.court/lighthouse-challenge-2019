const powerOn = () => { ship.powerOn = true }
const countModules = () => availableModules.length
const countEssential = () => availableModules.filter(m => m.essential).length
const loadModule = (index) => { 
  const m = availableModules[index]
  m.enabled = true
  ship.modules.push(m)
}
const findModuleIndex = (name) => availableModules.findIndex(m => m.name === name)
const resetLARRY = () => { for (let i=0; i<10; i+=1) { LARRY.quack() } }
const setMessage = () => { radio.message = JSON.stringify(navigation) }
const activateBeacon = () => { radio.beacon = true }
const setFrequency = () => { radio.frequency = (radio.range.low + radio.range.high) / 2 }
const initialize = () => {
  navigation.x = 0
  navigation.y = 0
  navigation.z = 0
}
const calibrateNav = (coord) => {
  while (true) {
    navigation[coord] = checkSignal()
    if (navigation[coord] !== undefined) {
      return
    }
  }
}
const calibrate = () => { calibrateNav('x'); calibrateNav('y'); calibrateNav('z'); }
const setSpeed = (speed) => { 
  const speedInt = parseInt(speed, 10)
  if (speedInt >= 0) {
    navigation.speed = speedInt
  }
}
const activateAntenna = () => { ship.antenna.active = true }
const sendBroadcast = () => {
  for (let i=0; i<100; i+=1) { broadcast() }
}
const configureBroadcast = () => {
  setFrequency()
  activateAntenna()
  sendBroadcast()
}

loadModule(findModuleIndex('life-support'))
loadModule(findModuleIndex('propulsion'))
loadModule(findModuleIndex('navigation'))
loadModule(findModuleIndex('communication'))
resetLARRY()
configureBroadcast()

