const powerOn = () => { ship.powerOn = true }
const countModules = () => availableModules.length
const countEssential = () => availableModules.filter(m => m.essential).length
const loadModule = (index) => { 
  const m = availableModules[index]
  m.enabled = true
  ship.modules.push(m)
}
const findModuleIndex = (name) => availableModules.findIndex(m => m.name === name)

loadModule(findModuleIndex('life-support'))
loadModule(findModuleIndex('propulsion'))

