const powerOn = () => { ship.powerOn = true }
const countModules = () => availableModules.length
const countEssential = () => availableModules.filter(m => m.essential).length
const loadModule = (index) => { 
  const m = availableModules[index]
  m.enabled = true
  ship.modules.push(m)
}
const findModuleIndex = (name) => availableModules.findIndex(m => m.name === name)
const resetLARRY = () => { for (let i=0; i<10; i+=1) { LARRY.quack() } }
const setMessage = () => { radio.message = JSON.stringify(navigation) }

loadModule(findModuleIndex('life-support'))
loadModule(findModuleIndex('propulsion'))
loadModule(findModuleIndex('navigation'))
loadModule(findModuleIndex('communication'))
resetLARRY()
setMessage()

